package com.cgi.servicenow.service;

import com.cgi.servicenow.api.TicketDto;
import com.cgi.servicenow.model.entity.TicketEntity;
import com.cgi.servicenow.model.repository.TicketRepository;
import com.cgi.servicenow.service.mapping.BeanMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    private TicketRepository ticketRepository;
    private BeanMapping beanMapping;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, BeanMapping beanMapping) {
        this.ticketRepository = ticketRepository;
        this.beanMapping = beanMapping
    }

    public TicketDto findByIdTicket(long idTicket) {
        return beanMapping.mapTo(ticketRepository.findByIdTicket(idTicket), TicketDto.class);
//        TicketEntity ticket = ticketRepository.findByIdTicket(idTicket)
//                .orElseThrow(RuntimeException::new);
//        return mapToDto(ticket);
    }

    public List<TicketDto> findByName(String name) {
        return beanMapping.mapTo(ticketRepository.findByName(name), TicketDto.class);
//       List<TicketEntity> tickets = ticketRepository.findByName(name);
//       List<TicketDto> ticketDtos = new ArrayList<>();
//       tickets.forEach(t -> {
//           ticketDtos.add(mapToDto(t));
//       });
//       return ticketDtos;

        //return this.findAllTickets()
        //        .stream()
        //        .filter(ticket -> ticket.getIdTickets() == id)
        //        .findFirst()
        //        .orElseThrow(() -> new RuntimeException("Group with id: " + id + " has not been found."));
    }

//    public TicketDto mapToDto(TicketEntity ticketEntity) {
//        TicketDto ticketDto = new TicketDto();
//        ticketDto.setIdTicket(ticketEntity.getIdTicket());
//        ticketDto.setName(ticketEntity.getName());
//        ticketDto.setCreatorId(ticketEntity.getCreatorId());
//        ticketDto.setPersonAssignedId(ticketEntity.getPersonAssignedId());
//        ticketDto.setCreationDatetime(ticketEntity.getCreationDatetime());
//        return ticketDto;
//    }

    @Override //upravit!
    public List<TicketDto> findAll() {

        List<TicketEntity> entities = ticketRepository.findAll();
        List<TicketDto> list = new ArrayList<>();

        for (TicketEntity ticketEntity: entities) {
            TicketDto ticketDto = new TicketDto();
            ticketDto.setIdTicket(ticketEntity.getIdTicket());
            ticketDto.setName(ticketEntity.getName());
            ticketDto.setCreatorId(ticketEntity.getCreatorId());
            ticketDto.setPersonAssignedId(ticketEntity.getPersonAssignedId());
            ticketDto.setCreationDatetime(ticketEntity.getCreationDatetime());
            list.add(ticketDto);
        }
        return list;
    }

    public void deleteByIdTicket(Long idTicket) {
        ticketRepository.deleteById(idTicket);
    }

    @Override
    public void createNewTicket(TicketDto ticketDto) {

    }

    @Override
    public void deleteByIdTicket(TicketDto ticketDto) {

    }
}
