package com.cgi.servicenow.service;

import com.cgi.servicenow.api.TicketDto;

import java.util.List;

public interface TicketService {
    List<TicketDto> findAll();

    TicketDto findByIdTicket(long id);

    List<TicketDto> findByName(String name);

    void createNewTicket(TicketDto ticketDto);

    void deleteByIdTicket(TicketDto ticketDto);
}
