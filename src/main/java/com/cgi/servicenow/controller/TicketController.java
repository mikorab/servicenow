package com.cgi.servicenow.controller;

import com.cgi.servicenow.api.TicketDto;
import com.cgi.servicenow.service.TicketServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/ticket")
public class TicketController {
    private TicketServiceImpl ticketServiceImpl;

    @Autowired
    public TicketController(TicketServiceImpl) {
        this.ticketServiceImpl = ticketServiceImpl;
    }

    // ~/persons?name=xyz
    @GetMapping
    public ResponseEntity<TicketDto> getByName(@RequestParam("name") String name) {
        return ResponseEntity.ok(ticketServiceImpl.findByName(name));
    }

    @DeleteMapping(path="/{id}")
    public void deleteByIdTicket(@PathVariable("id") Long id) {
        ticketServiceImpl.deleteByIdTicket(id);
    }
}
