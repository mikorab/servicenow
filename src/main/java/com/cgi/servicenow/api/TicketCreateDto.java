package com.cgi.servicenow.api;

import java.time.LocalDateTime;

//bez okamziteho prideleni resitele
public class TicketCreateDto {

    private Long idTicket;

    private String name;

    private Long creatorId;

    private LocalDateTime creationDatetime;
}
