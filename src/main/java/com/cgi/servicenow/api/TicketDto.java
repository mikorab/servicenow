package com.cgi.servicenow.api;


import java.time.LocalDateTime;

public class TicketDto {
    private Long idTicket;

    private String name;

    private Long creatorId;

    private Long personAssignedId;

    private LocalDateTime creationDatetime;

    public TicketDto() { }

    public TicketDto(Long idTicket, String name, Long creatorId, Long personAssignedId, LocalDateTime creationDatetime) {
        this.idTicket = idTicket;
        this.name = name;
        this.creatorId = creatorId;
        this.personAssignedId = personAssignedId;
        this.creationDatetime = creationDatetime;
    }

    public Long getIdTicket() {
        return idTicket;
    }

    public void setIdTicket(Long idTicket) {
        this.idTicket = idTicket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public Long getPersonAssignedId() {
        return personAssignedId;
    }

    public void setPersonAssignedId(Long personAssignedId) {
        this.personAssignedId = personAssignedId;
    }

    public LocalDateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(LocalDateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @Override
    public String toString() {
        return "TicketDto{" +
                "idTicket=" + idTicket +
                ", name='" + name + '\'' +
                ", creatorId=" + creatorId +
                ", personAssignedId=" + personAssignedId +
                ", creationDatetime=" + creationDatetime +
                '}';
    }
}
