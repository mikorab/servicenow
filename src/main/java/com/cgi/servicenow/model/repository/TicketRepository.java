package com.cgi.servicenow.model.repository;

import com.cgi.servicenow.model.entity.TicketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<TicketEntity, Long> {

    @Query("SELECT t FROM TicketEntity t WHERE t.idTicket = :idTicket")
    Optional<TicketEntity> findByIdTicket (@Param("id") Long idTicket);

    @Query("SELECT t FROM TicketEntity t WHERE t.name = :name")
    List<TicketEntity> findByName (@Param("name") String name);

    //@Modifying
    //@Query("DELETE FROM TicketEntity t WHERE t.idTicket = :id")
    void deleteByIdTicket (Long idTicket);




}

